internal = false

http_enabled = true

http_redirect = false

access_logs_enabled = false

alb_access_logs_s3_bucket_force_destroy = false

access_logs_region = "us-west-2"

cross_zone_load_balancing_enabled = false

http2_enabled = true

idle_timeout = 60

ip_address_type = "ipv4"

deletion_protection_enabled = false