output "public_subnet_cidrs" {
  value       = module.subnets.public_subnet_cidrs
  description = "Public subnet CIDRs"
}

output "private_subnet_cidrs" {
  value       = module.subnets.private_subnet_cidrs
  description = "Private subnet CIDRs"
}

output "vpc_id" {
  value = module.vpc.vpc_id
}

output "vpc_cidr" {
  value       = module.vpc.vpc_cidr_block
  description = "VPC CIDRN"
}

output "vpc_default_security_group_id" {
  value       = module.vpc.vpc_default_security_group_id
  description = "Default security group for VPC"
}

output "public_subnet_ids" {
  value       = module.subnets.public_subnet_ids
  description = "VPC Public Subnet IDs"
}

output "private_subnet_ids" {
  value       = module.subnets.private_subnet_ids
  description = "VPC Private Subnet IDs"
}

output "ecs_cluster_id" {
  value       = aws_ecs_cluster.default.id
  description = "ECS cluster ID"
}

output "ecs_cluster_arn" {
  value       = aws_ecs_cluster.default.arn
  description = "ECS cluster ARN"
}

output "alb_arn" {
  value       = aws_lb.default.arn
  description = "ALB ARN"
}

output "alb_dns_name" {
  value       = aws_lb.default.dns_name
  description = "DNS of the ALB"
}

output "http_listener_arn" {
  description = "The ARN of the HTTP forwarding listener"
  value       = join("", aws_lb_listener.http_forward.*.arn)
}

output "http_redirect_listener_arn" {
  description = "The ARN of the HTTP to HTTPS redirect listener"
  value       = join("", aws_lb_listener.http_redirect.*.arn)
}

output "https_listener_arn" {
  description = "The ARN of the HTTPS listener"
  value       = join("", aws_lb_listener.https.*.arn)
}

output "listener_arns" {
  description = "A list of all the listener ARNs"
  value = compact(
    concat(aws_lb_listener.http_forward.*.arn, aws_lb_listener.http_redirect.*.arn, aws_lb_listener.https.*.arn)
  )
}

output "redis_id" {
  value       = module.redis.id
  description = "Redis cluster ID"
}

output "redis_security_group_id" {
  value       = module.redis.security_group_id
  description = "Security group ID"
}

output "redis_port" {
  value       = module.redis.port
  description = "Redis port"
}

output "redis_endpoint" {
  value       = module.redis.endpoint
  description = "Redis primary endpoint"
}

output "redis_host" {
  value       = module.redis.host
  description = "Redis hostname"
}